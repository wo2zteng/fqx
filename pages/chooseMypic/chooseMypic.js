const device = wx.getSystemInfoSync();
const W = device.windowWidth;
const H = device.windowHeight - 50;

let cropper = require('../../welCropper/welCropper.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    taskInfo: {},
    clientHeight: 0,
    imagefile: '../../static/images/camera.png'
  },
  setContainerHeight: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          clientHeight: res.windowHeight
        });
      }
    });
  },
  onLoad: function (options) {
    this.setContainerHeight();
    cropper.init.apply(this, [W, H]);
  },
  changeAthumb: function () {
    var that = this

    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success(res) {
        const tempFilePath = res.tempFilePaths[0]
        console.log(tempFilePath)

        let modes = ["rectangle", "quadrangle"]
        let mode = modes[0]   //rectangle, quadrangle
        that.showCropper({
          src: tempFilePath,
          mode: mode,
          sizeType: ['original', 'compressed'],   //'original'(default) | 'compressed'
          callback: (res) => {
            if (mode == 'rectangle') {
              console.log("crop callback:" + res)
              that.hideCropper();
              that.setData({
                imagefile: res
              });
            }
            else {
              wx.showModal({
                title: '',
                content: JSON.stringify(res),
              })
            }
          }
        })
      }
    })
  },
  tonext: function () {
    wx.navigateTo({
      url: '../chooseTapic/chooseTapic'
    })
  }
})